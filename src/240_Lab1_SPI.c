/*
===============================================================================
 Name        : 240_Lab1_SPI.c
 Author      : Amit Pachore
 Version     : 1.0
 Description : Main definition to read/write data to flash
===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include "string.h"

//#include "spi.h"
#include "flash.h"

#define read_write_address 0x000000
#define num_byte 18
int main(void)
{
	uint8_t selection,i;
	char data[256]={0};
	Detect_Flash();
	while(1)
	{
		printf("Please select from below menu\n"
				"1.Write data to flash\n"
				"2.Read data from flash\n"
				"3.Quit\n");
		fflush(stdin);
		scanf("%d",&selection);
		fflush(stdin);

		switch(selection)
		{
		case 1:
			printf("Please enter data to write in flash:");
			fflush(stdin);
			fgets(data,256,stdin);
			if ((strlen(data)>0) && (data[strlen (data) - 1] == '\n'))
				data[strlen (data) - 1] = '\0';
			Write_Flash(read_write_address,&data[0],strlen(data));
			break;
		case 2:
			Read_Flash(read_write_address,&data[0],num_byte);
			printf("Data read from flash:");
			for(i=0;i<num_byte;i++)
			{
				printf("%c",data[i]);
			}
			printf("\n",data[i]);
			break;

		case 3: goto loop;
		break;
		}
	}
	loop: while(1);
	return 0 ;
}
