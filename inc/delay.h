/*
===============================================================================
 Name        : delay.h
 Author      : Amit Pachore
 Version     : 1.0
 Description : Function definitions for delay
===============================================================================
 */

#ifndef DELAY_H_
#define DELAY_H_

void delayMs(uint32_t delayInMs)
{
	LPC_TIM0->TCR = 0x02;        /* reset timer */
	LPC_TIM0->PR  = 0x00;        /* set prescaler to zero */
	LPC_TIM0->MR0 = delayInMs* (9000000 / 1000-1);
	LPC_TIM0->IR  = 0xff;        /* reset all interrrupts */
	LPC_TIM0->MCR = 0x04;        /* stop timer on match */
	LPC_TIM0->TCR = 0x01;        /* start timer */

	/* wait until delay time has elapsed */
	while (LPC_TIM0->TCR & 0x01);

	return;
}

#endif /* DELAY_H_ */
