/*
===============================================================================
 Name        : spi.h
 Author      : Amit Pachore
 Version     : 1.0
 Description : Function definitions for SPI configuration
===============================================================================
 */

#include <LPC17xx.h>

#ifndef SPI_H_
#define SPI_H_


void ssp_init(LPC_SSP_TypeDef *SSP)
{
	uint8_t ssp_number = (SSP == LPC_SSP1 ? 1 : 0);
	switch(ssp_number)
	{
	case 1:
		printf("SSP1 Initialization\n");
		LPC_SC -> PCONP |= (1<<10); //Enable SSP1 in Control Register
		//Peripheral Clock setup
		LPC_SC -> PCLKSEL0 &= ~(3 << 20); //Initial zero at 20 & 21 bits to make 00
		LPC_SC -> PCLKSEL0 &= ~(1 << 20); //01 selection for clock = CCLK

		//Pin Selection from MUX
		LPC_PINCON -> PINSEL0 &= ~(0xFF << 12);  //Pin selection made to zero, Pin P0.6 selected as GPIO
		LPC_PINCON -> PINSEL0 |= (0x2A << 14); // xx-10-10-10----- Pin select 10 for SCK1,MISO1,MOSI1

		//Configure P0.6 as Output for Chip select
		LPC_GPIO0 ->FIODIR |= (1<<6);

		break;

	case 0:
		printf("SSP0 Initialization\n");
		LPC_SC -> PCONP |= (1<<21); //Enable SSP0 in Control Register
		//Peripheral Clock setup
		LPC_SC -> PCLKSEL1 &= ~(3 << 10); //Initial zero at 10 & 11 bits to make 00
		LPC_SC -> PCLKSEL1 &= ~(1 << 10); //01 selection for clock = CCLK

		//Pin Selection from MUX
		LPC_PINCON -> PINSEL0 &= ~(0x3 << 30);  //Pin selection made to zero
		LPC_PINCON -> PINSEL0 |= (0x2 << 30); // SCK0 selection on PINSEL


		LPC_PINCON -> PINSEL1 &= ~(0x3F << 0);  //Pin selection made to zero
		LPC_PINCON -> PINSEL1 |= (0x28 << 0); // GPIO,MISO0 and MOSI0 selection on PINSEL

		//Configure P0.16 as Output for Chip select
		LPC_GPIO0 ->FIODIR |= (1<<16);
		break;

	default: printf("Please make correct SSP selection\n");
	break;

	}

	SSP -> CR0 = 7; //8 bit transfer mode
	SSP -> CR1 = (1<<1);//Enable SSP as Master

	SSP -> CPSR = 2; // Output Clock CCLK/2

	printf("%s initialized to 8 bit transfer mode, master device with clock sys_clock/2\n",SSP == LPC_SSP0 ? "SSP0":"SSP1");
}


char ssp_byte_exchange(LPC_SSP_TypeDef *SSP,char data)
{
	SSP -> DR = data;
	while (SSP -> SR & (1<<4));
	return SSP -> DR;
}

void chip_select(LPC_SSP_TypeDef *SSP)
{
	if (SSP == LPC_SSP1)
		LPC_GPIO0->FIOCLR = (1<<6);
	else if (SSP == LPC_SSP0)
		LPC_GPIO0->FIOCLR = (1<<16);
}

void chip_deselect(LPC_SSP_TypeDef *SSP)
{
	if (SSP == LPC_SSP1)
		LPC_GPIO0->FIOSET = (1<<6);
	else if (SSP == LPC_SSP0)
		LPC_GPIO0->FIOSET = (1<<16);
}
#endif /* SPI_H_ */
