/*
===============================================================================
 Name        : flash.h
 Author      : Amit Pachore
 Version     : 1.0
 Description : Function definitions for flash use
===============================================================================
 */

#ifndef FLASH_H_
#define FLASH_H_

#include <stdio.h>

#include "stdlib.h"

#include "spi.h"
#include "delay.h"
#include "stdbool.h"


#define WB2564_MFG_ID 0xEF
#define WB2564_DEV_ID 0x4017


typedef enum {

	OPCODE_READ_DEV_ID    = 0x9F,
	OPCODE_READ_DATA      = 0x03,
	OPCODE_WR_ENABLE      = 0x06,
	OPCODE_WR_PAGE		  = 0x02,
	OPCODE_4K_SECT_ERASE  = 0x20,
	OPCODE_32K_BLK_ERASE  = 0x52,
	OPCODE_CHIP_ERASE 	  = 0xC7,
	OPCODE_READ_STA_REG_1 = 0x05,

	Dummy_byte			  = 0xFF

} flash_opcodes;

void Write_Enable()
{
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_WR_ENABLE);
	chip_deselect(LPC_SSP1);
	delayMs(1);

}

void Detect_Flash()
{
	uint8_t MFG_ID;
	uint16_t DEV_ID;
	ssp_init(LPC_SSP1);
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_READ_DEV_ID);
	MFG_ID = ssp_byte_exchange(LPC_SSP1,Dummy_byte);
	DEV_ID = (ssp_byte_exchange(LPC_SSP1,Dummy_byte)<<8);
	DEV_ID |= ssp_byte_exchange(LPC_SSP1,Dummy_byte);
	chip_deselect(LPC_SSP1);

	if ((DEV_ID == WB2564_DEV_ID)&&(MFG_ID == WB2564_MFG_ID))
		printf("Flash Detected\n");
	else
		printf("Flash not detected\n");
}

bool Flash_Busy()
{
	uint8_t status;
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_READ_STA_REG_1);
	status = ssp_byte_exchange(LPC_SSP1,Dummy_byte);
	chip_deselect(LPC_SSP1);
	printf("Status: %x\n",status);
	return (status & (1<<0));
}

void Flash_4KSector_Erase(uint32_t base_address)
{
	Write_Enable();
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_4K_SECT_ERASE);
	ssp_byte_exchange(LPC_SSP1,(base_address>>16));
	ssp_byte_exchange(LPC_SSP1,(base_address>>8));
	ssp_byte_exchange(LPC_SSP1,(base_address>>0));

	chip_deselect(LPC_SSP1);

	printf("4KB sector erased starting at address 0x%06x\n",base_address);

	while(Flash_Busy());
}

void Read_Flash(uint32_t base_address, char * data, uint8_t bytes_to_read)
{
	uint8_t i;
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_READ_DATA);
	ssp_byte_exchange(LPC_SSP1,(base_address>>16));
	ssp_byte_exchange(LPC_SSP1,(base_address>>8));
	ssp_byte_exchange(LPC_SSP1,(base_address>>0));

	for(i=0;i<bytes_to_read;i++)
	{
		data[i] = ssp_byte_exchange(LPC_SSP1,Dummy_byte);
	}

	chip_deselect(LPC_SSP1);

	printf("%d bytes starting at address 0x%06x read from flash\n",bytes_to_read,base_address);
}

void Write_Flash(uint32_t base_address, char * data, uint8_t bytes_to_write)
{
	Flash_4KSector_Erase(base_address);
	uint8_t i;
	Write_Enable();
	chip_select(LPC_SSP1);
	ssp_byte_exchange(LPC_SSP1,OPCODE_WR_PAGE);
	ssp_byte_exchange(LPC_SSP1,(base_address>>16));
	ssp_byte_exchange(LPC_SSP1,(base_address>>8));
	ssp_byte_exchange(LPC_SSP1,(base_address>>0));

	for(i=0;i<bytes_to_write;i++)
	{
		ssp_byte_exchange(LPC_SSP1,data[i]);
	}

	chip_deselect(LPC_SSP1);

	printf("%d bytes writing finished to flash at address 0x%06x\n",bytes_to_write,base_address);
}




#endif /* FLASH_H_ */
